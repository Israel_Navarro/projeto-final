<?php /* Template Name: Checkout */ ?>
<div id="opacity"></div>
<?php get_header(); ?>
<body>

<?php 
        if(have_posts()){
            while(have_posts()){
                the_post();
            
    ?>
    <h1><?php the_title(); ?></h1>
    <main><?php the_content(); ?></main>
    <?php 
    }
}
    ?>
    
</html>
<?php get_footer(); ?>
</body>