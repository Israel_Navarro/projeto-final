<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>./style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@300;500&display=swap" rel="stylesheet">
    <title><?php bloginfo( 'name' ) ?><?php wp_title( '|' ); ?></title>
    <?php wp_head(); ?>
</head>
<header>
    <div class="search-logo">
        <a href="http://projeto-final.local/pagina-inicial/">
            <img src="<?php echo get_stylesheet_directory_uri() ?>./img/logo-projeto-final (1).png" alt="">
        </a>
        <div class="search-box">
            <form class = "pesquisa" action="<?php echo bloginfo("url");?>/shop/" method="get">
                <div class="lupinha">
                    <input type="image" src="<?php echo get_stylesheet_directory_uri()?>/img/lupa.png" width=30 height=29>
                </div>
                <input type="text" name="s" cat="s" placeholder="Pesquisar..." value="<?php echo the_search_query( )?>" id="search" >
                <input type="hidden" name="post-type" value="product">
            </form>
        </div>
    </div>

    <div class="carrinho-pedidos-user">
        <a class="fazer-pedido" href="http://projeto-final.local/shop/">Faça um pedido</a>
        <div id="mySidenav" class="sidenav">
            
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <?php echo '<h1 class="tit-cart">CARRINHO</h1>';?>
            <?php echo '<div class="linha-cart"></div>';?>
            <?php
                $items = WC()->cart->get_cart();
                foreach($items as $item => $values) { 
                    $_product =  wc_get_product( $values['data']->get_id());
                    $id = $values['data']->get_id();
                    echo '<div class="product-cart">';
                        echo '<div class="imagem-product-cart">'.$_product->get_image().'</div>';
                        echo '<div>';
                            echo '<div class="nome-product-cart">'.$_product->get_title().'</div>';
                            echo '<div class="preco-quant">';
                                echo '<div class="muda-quant">';
                                    echo '<a class="add-sub" href="">-</a>';
                                    echo '<div class="quant-product-cart">'.$values['quantity'].'</div>';
                                    $protocolo = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=="on") ? "https" : "http");
                                    $url = '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                    echo '<a class="add-sub" href="'.$protocolo.$url.'?add-to-cart=' . $id . '">+</a>';
                                echo '</div>';
                                echo '<div class="preco-product-cart">R$ '.$_product->get_price().'</div>';
                            echo '</div>';
                            
                        echo '</div>';
                    echo '</div>'; 
                }
            ?> 
            <?php echo '<div class="linha-cart"></div>';?>
            <?php echo '<p class="quant-cart">Total do Carrinho: '.WC()->cart->get_cart_total().'</p>';?>
            <a class="cart-customlocation" href="http://projeto-final.local/checkout/" title="<?php _e( 'View your shopping cart' );?>">COMPRAR</a>
            
            
        </div>
        <span onclick="openNav()" class=botao-cabecalho>
                    <div class="carrinho">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>./img/carrinho.png" width=50 height=38 href="">
                    </div>
        </span>
        <a class=botao-cabecalho href="http://projeto-final.local/my-account/">
                    <div class="user">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>./img/user.png" width=41 height=40 href="">
                    </div>
        </a>
    </div>
    
 
    
    
</header>