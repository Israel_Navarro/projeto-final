<section class="conheca-loja">
    <h1 class="nome4">SELECIONE UMA CATEGORIA</h1>
    <div class = "tipos-pratos">
        <?php

            $taxonomy     = 'product_cat';
            $orderby      = 'name';  
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no  
            $title        = '';  
            $empty        = 0;

            $args = array(
                    'taxonomy'     => $taxonomy,
                    'orderby'      => $orderby,
                    'show_count'   => $show_count,
                    'pad_counts'   => $pad_counts,
                    'hierarchical' => $hierarchical,
                    'title_li'     => $title,
                    'hide_empty'   => $empty
            );
            $all_categories = get_categories( $args );
            foreach ($all_categories as $cat) {
                if($cat->category_parent == 0) {
                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                    $category_id = $cat->term_id;
                    $image = wp_get_attachment_image( $thumbnail_id );
                    $image_url = wp_get_attachment_url( $thumbnail_id );
                    echo '<div class="tamanho"><br /><a class="imagens-cat" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="nome-cat">'.$cat->name.'</p></a></div>';
                    
                }       
            }
        ?>
    </div>
    <h1 class="nome4">PRATOS</h1>
<?php 
        if(have_posts()){
            while(have_posts()){
                the_post();
            
    ?>
    <h1><?php the_title(); ?></h1>
    <main><?php the_content(); ?></main>
    <?php 
    }
}
    ?>
</section>





<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////// -->






add_action('woocommerce_account_dashboard',editaDados());
    editaDados(){

        // /**
        //  * Edit account form
        //  *
        //  * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
        //  *
        //  * HOWEVER, on occasion WooCommerce will need to update template files and you
        //  * (the theme developer) will need to copy the new files to your theme to
        //  * maintain compatibility. We try to do this as little as possible, but it does
        //  * happen. When this occurs the version of the template file will be bumped and
        //  * the readme will list any important changes.
        //  *
        //  * @see https://docs.woocommerce.com/document/template-structure/
        //  * @package WooCommerce\Templates
        //  * @version 3.5.0
        //  */

        // defined( 'ABSPATH' ) || exit;

        // do_action( 'woocommerce_before_edit_account_form' );

        // echo' <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >';

        // do_action( 'woocommerce_edit_account_form_start' );

        // echo'<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">';
        //     echo'<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>';
        //     echo'<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" />';
        // echo'</p>';
        // echo'<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">';
        //     echo'<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>';
        //     echo'<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />';
        // echo'</p>';
        // echo'<div class="clear"></div>';

        // echo'<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">';
        //         echo'<label for="account_display_name"><?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>';
        //         echo'<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo esc_attr( $user->display_name ); ?>" /> <span><em><?php esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'woocommerce' ); ?></em></span>';
        //     echo'</p>';
        //     echo'<div class="clear"></div>';

        //     echo'<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">';
        //         echo'<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>';
        //         echo'<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />';
        //     echo'</p>';

        //     echo'<fieldset>';
        //     echo'<legend><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>';

        //     echo'<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">';
        //         echo'<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>';
        //         echo'<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" />';
        //     echo'</p>';
        //     echo'<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">';
        //         echo'<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>';
        //         echo'<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />';
        //     echo'</p>';
        //     echo'<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">';
        //         echo'<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>';
        //         echo'<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />';
        //     echo'</p>';
        //     echo'</fieldset>';
        //     echo'<div class="clear"></div>';

        //     do_action( 'woocommerce_edit_account_form' );

        //     echo'<p>';
        //         echo wp_nonce_field( 'save_account_details', 'save-account-details-nonce' );
        //         echo '<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>';
        //         echo '<input type="hidden" name="action" value="save_account_details" />';
        //     echo'</p>';

        //     do_action( 'woocommerce_edit_account_form_end' );
        // echo'</form>';

        // do_action( 'woocommerce_after_edit_account_form' );

            }