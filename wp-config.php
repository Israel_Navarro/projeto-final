<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'KHtz6K7OdM6fgKXyJYwASGajx1f99GZV9gHi6zpZNv7xblDH8RB6OJjt6vXe8iIaLIi3DKSFhBQ3iiWk3S7ijA==');
define('SECURE_AUTH_KEY',  '570m81Edp5GjNpN6KvQY4i5vkNGgDOToiL3q934DuqiWT3JD+uHJifrwIlbbqRXcW5WrEgnYBQlg4S2LaUXKxw==');
define('LOGGED_IN_KEY',    'h1Y9cQXeToaBMaMOwxG092sVNMA02g0q1uH6iv8jmRDostSBTTs4TD75DqkVktJPz/AG4AcIcALlT5knVVifKQ==');
define('NONCE_KEY',        'IEsVwRFJLT0uZr3Tb/4CCioQifyJLTca+GP4aouFGp/RFkfwLfNwQjTYoOoIT1m0RA1f9gDcrRK1OyfhZeENHw==');
define('AUTH_SALT',        'd0TaM9548BvGETnlxCyFBP+nSD1v7ejl6QCnhIF9dK9vSlnQh4/VprKPczIWZg6HbAjOYbTTHz4krET3qbDVEw==');
define('SECURE_AUTH_SALT', '0/g/wShm1cW09Bm499tdxUJxRjA9pAE9suhDDSXQtkyXJIRmcjw8/XbE8JiQCuCPR2y2U89eTCevwBqNWFnYxg==');
define('LOGGED_IN_SALT',   'SCHaCPCOQKLWDjMr99WEo9WULEGtkxVyIhtdO8gXvBPOOu88bcmvHcbDIFHk9dFgz5QQfFCd+Pr0wNW6T90vEA==');
define('NONCE_SALT',       'FxxWN/Pw1iz0nd2Brd3BiyvAw6XfP4RjybbBpUryL9pC8sf+0/pGhMk/ziHkPBoa3Hf+vHAQFBqyrXZ0cb8/Qg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
