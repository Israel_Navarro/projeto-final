<?php
    
    function my_theme_scripts_function() {
        wp_enqueue_script( 'cart',get_template_directory_uri().'/js/cart.js');
    }
    add_action( 'wp_enqueue_scripts', 'my_theme_scripts_function');

    function mytheme_add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
    

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// Shop ////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////


    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10, 0);
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

    add_action('woocommerce_before_shop_loop', 'open_div');
    function open_div() {
        echo '<div class="show-products">';
    }
    add_action('woocommerce_after_shop_loop', 'close_div');
    function close_div() {
        echo '</div>';
    }

    remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title',10,0);
    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price',10,0);
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
    add_action('woocommerce_shop_loop_item_title', 'cria_tudo');
    function cria_tudo() {
        echo        '<div class="nome-prato-dia-shop">';
        echo            '<p class="nome-produto-shop">'.woocommerce_template_loop_product_title().'</p>';
        echo            '<div class="compra">';
        echo                '<p class="valor-produto">'.woocommerce_template_loop_price().'</p>';
        echo                '<img class="carrinho-compra" src="' . get_stylesheet_directory_uri() . '/img/carrinho-compra (1).png" width="50" height="45">';          
        echo            '</div>';
        echo        '</div>';
    }
    
    add_action('woocommerce_after_shop_loop_item', 'aumenta_espace');
    function aumenta_espace() {
        echo '<div class="aumenta_space">';
        echo '</div>';
    }


    add_action( 'woocommerce_before_shop_loop', 'criaCat');
    function criaCat(){
        echo '<section class="categorias-loja">';
        echo '<h1 class="nome4">SELECIONE UMA CATEGORIA</h1>';
        echo '<div class = "tipos-pratos">';
            $taxonomy     = 'product_cat';
            $orderby      = 'name';  
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no  
            $title        = '';  
            $empty        = 0;

            $args = array(
                    'taxonomy'     => $taxonomy,
                    'orderby'      => $orderby,
                    'show_count'   => $show_count,
                    'pad_counts'   => $pad_counts,
                    'hierarchical' => $hierarchical,
                    'title_li'     => $title,
                    'hide_empty'   => $empty
            );
            $all_categories = get_categories( $args );
            foreach ($all_categories as $cat) {
                if($cat->category_parent == 0) {
                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                    $category_id = $cat->term_id;
                    $image = wp_get_attachment_image( $thumbnail_id );
                    $image_url = wp_get_attachment_url( $thumbnail_id );
                    $protocolo = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=="on") ? "https" : "http");
                    $url = '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                    if ($protocolo.$url === get_term_link($cat->slug, 'product_cat')){
                        echo '<div class="tamanho"><div class="borda-roxa"><br /><a class="imagens-cat" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="nome-cat">'.$cat->name.'</p></a></div></div>';
                    }
                    else{
                        echo '<div class="tamanho"><br /><a class="imagens-cat" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="nome-cat">'.$cat->name.'</p></a></div>';
                    }
                    
                }       
            }
        echo '</div>';
        echo '<h1 class="nome5">PRATOS</h1>';

        $protocolo = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=="on") ? "https" : "http");
        $url = '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        switch ($protocolo.$url) {
            case "http://projeto-final.local/product-category/japonesa/":
                echo "COMIDA JAPONESA";
                break;
            case "http://projeto-final.local/product-category/massas/":
                echo "MASSAS";
                break;
            case "http://projeto-final.local/product-category/nordestina/":
                echo "COMIDA NORDESTINA";
                break;
            case "http://projeto-final.local/product-category/vagana/":
                echo "COMIDA VEGANA";
                break;
            case "http://projeto-final.local/shop/":
                echo "TODOS PRATOS";
                break;
        };
        
        echo '<div class="filtros">';

        echo '<div class="ordenacao">';
            echo '<p class="titulo-filtro">Ordenar por</p>';
            echo woocommerce_catalog_ordering();
        echo '</div>';
        
        /**
         * The template for displaying product price filter widget.
         *
         * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-price-filter.php
         *
         * HOWEVER, on occasion WooCommerce will need to update template files and you
         * (the theme developer) will need to copy the new files to your theme to
         * maintain compatibility. We try to do this as little as possible, but it does
         * happen. When this occurs the version of the template file will be bumped and
         * the readme will list any important changes.
         *
         * @see     https://docs.woocommerce.com/document/template-structure/
         * @package WooCommerce\Templates
         * @version 3.7.1
         */

        defined( 'ABSPATH' ) || exit;

        
        do_action( 'woocommerce_widget_price_filter_start', $args );

        echo '<form method="get" action="' . esc_url( $form_action ) . '">';
        echo    '<div class="price_slider_wrapper">';
        echo        '<div class="price_slider" style="display:none;"></div>';
        echo        '<p class="titulo-filtro">Filtro de preço:</p>';
        echo        '<div class="price_slider_amount" data-step="' . esc_attr( $step ) . '">';
        echo            '<div class="filtro-preco">';
            echo            '<p>De:</p>';
            echo            '<input type="text" id="min_price" name="min_price" value="' . esc_attr( $current_min_price ) . '" data-min="' . esc_attr( $min_price ) . '"  />';
            echo            '<p>Até:</p>';
            echo            '<input type="text" id="max_price" name="max_price" value="' . esc_attr( $current_max_price ) . '" data-max="' . esc_attr( $max_price ) . '"  />';
        echo            '</div>';
        
                        /* translators: Filter: verb "to filter" */
        echo            '<button type="submit" class="button-filtro">' . esc_html__( 'Filtrar', 'woocommerce' ) . '</button>';
        echo            '<div class="price_label" style="display:none;">';
        echo                esc_html__( 'Price:', 'woocommerce' ) . '<span class="from"></span> &mdash; <span class="to"></span>';
        echo            '</div>';
        echo            wc_query_string_form_fields( null, array( 'min_price', 'max_price', 'paged' ), '', true );
        echo            '<div class="clear"></div>';
        echo        '</div>';
        echo    '</div>';
        echo '</form>';

        do_action( 'woocommerce_widget_price_filter_end', $args );
        echo '</div>';
        echo '</section>';

    }



///////////////////////////////////////////////////////////////////////////////////////////////////////
//////// Single Product //////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
    
    add_theme_support( 'wc-product-gallery-zoom' );
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_add_to_cart',30,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10,0);
    remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40,0);

    remove_action('woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',10,0);
    
    add_action('woocommerce_before_single_product_summary', 'open_div2');
    function open_div2() {
        echo '<div class="produtos">';
    }
    
    add_action('woocommerce_after_single_product_summary','mostraDados');
    function mostraDados(){
        echo '<div class="mostra-dados">';
        echo '<div>'.woocommerce_template_single_title().'</div>';
        echo '<p>'.woocommerce_template_single_excerpt().'</p>';
        echo '<div>'.woocommerce_template_single_add_to_cart().'</div>';
        echo '</div>';
        echo '</div>';

        global $product;
        $id = $product->get_id();

        $args = array(
            'orderby' => 'name',
        );
        $product_cats = wp_get_post_terms( $product->get_id(), 'product_cat', $args );
       $cat_properties = get_object_vars($product_cats[0]);

       switch ($cat_properties['slug']) {
        case 'vagana':
            echo '<h3 class ="mais-comida">MAIS COMIDA VEGANA</h3>';
            break;
        
        case 'japonesa':
            echo '<h3 class ="mais-comida">MAIS COMIDA JAPONESA</h3>';
            break;

        case 'massas':
            echo '<h3 class ="mais-comida">MAIS MASSAS</h3>';
            break;
        
        case 'nordestina':
            echo '<h3 class ="mais-comida">MAIS COMIDA NORDESTINA</h3>';
            break;
    }
    }
   
?>
