<div id="opacity"></div>
<?php get_header(); ?>
<body class="base">
<?php 
    if(have_posts()){
        while(have_posts()){
            the_post();
            
    ?>
    <main><?php the_content(); ?></main>
    <?php 
    }
}
    ?>
</html>
<?php get_footer(); ?>
</body>