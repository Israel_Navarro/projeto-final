<?php /* Template Name: Página Inicial */ ?>
<div id="opacity"></div>
<?php get_header(); ?>
<body>

<!-- ////////////////////// Apresentação //////////////////////////////////////////////// -->
<!-- ///////////////////////////////////////////////////////////////////////////////////// -->

<section class="apresentacao">
            <h1 class="nome">Comes&Bebes</h1>
            <h3 class="subtitulo">O restaurante para todas as fomes</h3>
        </section>
        
<!-- ////////////////////// Conheça a Loja //////////////////////////////////////////////// -->
<!-- ///////////////////////////////////////////////////////////////////////////////////// -->
<section class="conheca-loja">
    <h1 class="nome2">CONHEÇA NOSSA LOJA</h1>
    <h1 class="pratos-categorias">Tipos de pratos principais</h1>

    <div class = "tipos-pratos">
        <?php

            $taxonomy     = 'product_cat';
            $orderby      = 'name';  
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no  
            $title        = '';  
            $empty        = 0;

            $args = array(
                    'taxonomy'     => $taxonomy,
                    'orderby'      => $orderby,
                    'show_count'   => $show_count,
                    'pad_counts'   => $pad_counts,
                    'hierarchical' => $hierarchical,
                    'title_li'     => $title,
                    'hide_empty'   => $empty
            );
            $all_categories = get_categories( $args );
            foreach ($all_categories as $cat) {
                if($cat->category_parent == 0) {
                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                    $category_id = $cat->term_id;
                    $image = wp_get_attachment_image( $thumbnail_id );
                    $image_url = wp_get_attachment_url( $thumbnail_id );
                    echo '<div class="tamanho"><br /><a class="imagens-cat" href="'. get_term_link($cat->slug, 'product_cat') .'">'.$image .'<p class="nome-cat">'.$cat->name.'</p></a></div>';
                    
                }       
            }
        ?>
    </div>
    
    <h1 class="pratos-do-dia">Pratos do dia de hoje:</h1>
    <div class="dia">
        <?php   
            setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');
            $day=strftime("%u",time());
            switch ($day) {
                case 7:
                    echo "DOMINGO";
                    break;
                case 1:
                    echo "SEGUNDA";
                    break;
                case 2:
                    echo "TERÇA";
                    break;
                case 3:
                    echo "QUARTA";
                    break;
                case 4:
                    echo "QUINTA";
                    break;
                case 5:
                    echo "SEXTA";
                    break;
                case 6:
                    echo "SÁBADO";
                    break;
                
            }
            ?>
    <div class = pratos-dia-conteudo>
        <?php 
            $args = array(
                'post_type'      => 'product',
                'posts_per_page' => 4,
                'product_tag'    => $day,
                'status' => 'publish',
                'limit'  => -1 

            ); 
            $products_all_tag = wc_get_products($args);
            foreach ($products_all_tag as $product) {
                $product_id = $product->get_id();
                $product_tags =  wc_get_product_tag_list( $product_id, $sep = ' ', $before = '', $after = '' );
                $product_price = $product->get_price();
                $product_tag =  wc_get_product_tag_list($product_id, $sep = ', ', $before = '', $after = '');
                $image = woocommerce_get_product_thumbnail();
                echo '<div class="tamanho">';
                echo    '<br />';
                echo    '<a class="imagens-cat" href="http://projeto-final.local/product/'.$product->slug.'/">'.$image.'</a>';     
                echo        '<div class="nome-prato-dia">';
                echo            '<p class="nome-produto">'.$product->name.'</p>';
                echo            '<div class="compra">';
                echo                '<p class="valor-produto">R$ '.$product_price.'</p>';
                echo                '<a class="carrinho-compra" href="http://projeto-final.local/product/'.$product->slug.'/"><img src="' . get_stylesheet_directory_uri() . '/img/carrinho-compra (1).png" width="50" height="45"></a>';           
                echo            '</div>';
                echo        '</div>';
                echo '</div>';
                
                
            }
        ?>
    </div>      
    </div> 
    <a class="ver-outras-opcoes" href="http://projeto-final.local/shop/">Veja outras opções</a>
</section>

<!-- ////////////////////// Visite a Loja Física //////////////////////////////////////////////// -->
<!-- ///////////////////////////////////////////////////////////////////////////////////// -->
<section class="loja-fisica">
    <h1 class="nome3">VISITE NOSSA LOJA FÍSICA</h1>
    <div class="conteudo-loja-fisica">
        <div class="parte1-conteudo">
            <div class="mapa"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1928043754892!2d-43.136297684992726!3d-22.906258343611036!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%20-%20Boa%20Viagem%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1633111634450!5m2!1spt-BR!2sbr" width="345" height="203" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
            <div class="contato">
                <img src="<?php echo get_stylesheet_directory_uri() ?>./img/talheres.jpg" alt="" width=40 height=38>
                <p class="contato-texto">Av. Milton Tavares de Souza - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</p>
            </div>
            <div class="contato">
                <img src="<?php echo get_stylesheet_directory_uri() ?>./img/telefone.jpg" alt="" width=40 height=38>
                <p class="contato-texto">(99) 99999-9999</p>
            </div>
        </div>
        
        <ul class="slider3">
            <li>
                <input type="radio" id="slide11" name="slidef" checked>
                <label for="slide11"></label>
                <img src="<?php echo get_stylesheet_directory_uri() ?>./img/pessoas-comendo5.jpg" alt="" width=593 height=349>
            </li>
            <li>
                <input type="radio" id="slide12" name="slidef" checked>
                <label for="slide12"></label>
                <img src="<?php echo get_stylesheet_directory_uri() ?>./img/pessoas-comendo2.jpg" alt="" width=593 height=349>
            </li>
            <li>
                <input type="radio" id="slide13" name="slidef" checked>
                <label for="slide13"></label>
                <img src="<?php echo get_stylesheet_directory_uri() ?>./img/pessoas-comendo3.jpg" alt="" width=593 height=349>
            </li>
            </ul>
    </div>
        
</section>
    
</html>
<?php get_footer(); ?>
</body>

            